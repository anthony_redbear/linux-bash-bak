# .bashrc

# Source global definitions
if [ -f /etc/bashrc ]; then
	. /etc/bashrc
fi

# Uncomment the following line if you don't like systemctl's auto-paging feature:
# export SYSTEMD_PAGER=
export PATH=$PATH:/home/anthony/virtualenvs/pelican/bin

# User specific aliases and functions
alias c="clear;pwd;ls"
alias l="clear"
alias v='vim'
alias vi='sudo vim'
alias f='cd ..;clear;pwd;ls'
alias vb='vi ~/.bashrc'
alias sb='source ~/.bashrc'
alias sc='sudo systemctl'
alias scs='sudo systemctl status'
alias sce='sudo systemctl enable'
alias scr='sudo systemctl restart'
alias vp="vi /etc/profile"
alias sp="source /etc/profile"

# yum  rpm
alias ys='sudo yum search'
alias yi='sudo yum install'
alias yg='sudo yum group'
alias ygl='sudo yum grouplist'
alias ygi='sudo yum groupinfo'
alias rg='rpm -qa | grep'

# git
alias ga="git add"
alias gc="git commit"
alias gcl="git clone"
alias gs="git status"
alias gd="git diff"
alias gl="git lg"
alias gb="git branch"
alias gbv="git branch -v"
alias gbr="git branch -r"
alias gba="git branch -a"
alias gbav="git branch -av"
alias gco="git checkout"
alias gcob="git checkout -b"
alias gcom="git checkout master"
alias gr="git remote"
alias gpush="git push"
alias gf="git fetch"
alias gm="git merge"
alias gcfg="git config"
alias gcfgl="git config --list"

# blog
alias mg="make github"
alias mh="make html"
alias blog="cd ~/blog;c"
alias pc="v ~/blog/pelicanconf.py"

# find grep
alias grpr="grep -r -i"
alias grpl="grep -r -i -l"
alias fn="find ./ -name"

# Espruino
alias cde="cd /home/share/Espruino;clear;pwd;ls"
alias mc="make clean"
alias mk="make"
alias em="cd /home/share/Espruino;ls; ESPRUINO_1V3=1 RELEASE=1 make"
alias ems="cd /home/share/Espruino;ls; sudo ESPRUINO_1V3=1 RELEASE=1 make serialflash"
alias l0="sudo python scripts/stm32loader.py  -ew -k -a 0x8000000 -p /dev/ttyACM0"
alias l2="sudo python scripts/stm32loader.py  -ew -k -a 0x8002800 -p /dev/ttyACM0"
